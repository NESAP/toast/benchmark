# How to run the TOAST example scripts and collect the FOM

TOAST requires several dependencies.  The full installation procedure is described in the [online documentation](https://toast-cmb.readthedocs.io/en/latest/).

Assuming that TOAST and its dependencies are installed successfully, one can run the representative test cases (on Cori-Haswell) with

```
$ toast> cd examples
$ examples> ./generate_slurm.sh
$ examples> sbatch representative_satellite_cori-haswell.slurm
$ examples> sbatch representative_ground_cori-haswell.slurm
```

The jobs will produce output directories such as `out_representative_satellite_cori-haswell` and `out_representative_ground_multisite_cori-haswell`.  Using TOAST commit `5e54f3f8c603c3b3da428b4c6aed7c00774eb507` the directories will include files called `log` and `timing_report_main.out`.  The `log` is the main log file for collecting outputs from all of the modules run as a part of the TOAST pipeline while `timing_report_main.out` collects per-process time and memory profiles.  The format of both of these files is subject to change.  Because of their size, the `timing_report_main.out` files were trimmed to the first 1000 lines.  The main `log` files also included a significant amount of unnecessary repetition so they were trimmed with
```
for f in out*/log; do grep -v 'Channel name' $f | grep -v ' | 0' | grep -v 'Simulating dipole' | grep -v 'Writing serialization file' | grep -v 'Sigma is' | grep -v 'fwhm is' > log; mv log $f; done
```

The time taken to execute individual pipeline modules such as noise and signal simulation and mapmaking are interesting in their own right.  However, to establish a single figure of merit, we propose the total wall time it takes to complete the ground and satellite simulations.  For the moment we extract this information from the top of `timing_report_main.out` files.  We find:

| System | Satellite example (314 nodes) | Ground example (64 nodes) | Total | MPI tasks per node | OpenMP threads per process |
| ------ | -----------------------------:| -------------------------:| -----:| ------------------:| --------------------------:|
| Edison |                         1648s |                     1018s | 2666s |                  8 |                          3 |
| Cori-Haswell |                    540s |                      788s | 1328s |                 16 |                          2 |

The representantive examples are *guaranteed* to evolve before Perlmutter becomes available.  For this reason, we provide the Edison and Cori numbers to establish a TOAST-specific scaling factor (2666/1328=2.007) between the two systems.  This will allow scaling the FOM numbers for future versions of the example pipelines run on Cori-Haswell.
